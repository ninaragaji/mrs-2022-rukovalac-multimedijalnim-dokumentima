import sys

from PySide2.QtGui import QFont
from PySide2.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QPushButton, QVBoxLayout, QDesktopWidget


class PopupWindow(QMainWindow):
    def __init__(self,title,content):
        super().__init__()
        self.setWindowTitle(title)
        self.setFixedSize(300, 150)
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        # Create the contents of the popup window
        widget = QWidget(self)
        layout = QVBoxLayout(widget)

        label = QLabel(content)
        label.setFont(QFont('Arial',13))
        layout.addWidget(label)

        close_button = QPushButton("Close")
        close_button.clicked.connect(self.close)
        layout.addWidget(close_button)

        self.setCentralWidget(widget)

        self.show()