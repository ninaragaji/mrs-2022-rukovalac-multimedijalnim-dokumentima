from PySide2 import QtWidgets, QtGui
from PySide2.QtGui import QImage, QPalette, QPixmap
from PySide2.QtMultimedia import QMediaPlayer
from PySide2.QtMultimediaWidgets import QVideoWidget
from PySide2.QtWidgets import (QAction, QApplication, QFileDialog, QLabel,
                               QMainWindow, QMenu, QMessageBox, QScrollArea, QSizePolicy, QPushButton, QHBoxLayout)
from ui.audio_widget import AudioWidget

from ui.video_widget import VideoWidget


class WorkspaceWidget(QtWidgets.QWidget):

    def __init__(self, parent, video_path=None, audio_path=None):
        super().__init__(parent)
        self.video_path = video_path
        self.audio_path = audio_path

        self.video_widget = None
        self.audio_widget = None
        self.main_layout = QtWidgets.QGridLayout()

        # self.main_text = QtWidgets.QTextEdit()
        # self.main_layout.addWidget(self.main_text)

        self.tool_bar = QtWidgets.QToolBar("Toolbar", self)

        self.toolbar_actions = {
            "text": QtWidgets.QAction(QtGui.QIcon("resources/icons/text_icon.png"), "&Text", self),
            "image": QtWidgets.QAction(QtGui.QIcon("resources/icons/image_icon.png"), "&Image", self),
            "delete": QtWidgets.QAction(QtGui.QIcon("resources/icons/delete_icon.png"), "&Delete", self),
            "video": QtWidgets.QAction(QtGui.QIcon("resources/icons/video_icon.png"), "&Video", self),
            "audio": QtWidgets.QAction(QtGui.QIcon("resources/icons/audio_icon.png"), "&Audio", self)
        }

        self.bind_actions()
        self.populate_tool_bar()

        self.setLayout(self.main_layout)

    def create_tab_widget(self):
        self.tab_widget = QtWidgets.QTabWidget(self)
        self.tab_widget.setTabClosable(True)
        self.tab_widget.tabCloseRequested.connect(self.delete_tab)

    def delete_tab(self, index):
        self.tab_widget.removeTab(index)

    
    def create_text_slot(self):
        self.text_widget = QtWidgets.QTextEdit()
        self.text_widget.setText("Ovo je slot za rad sa tekstom.")
        self.text_widget.setFixedSize(300, 100)
       # self.text_widget.mouseDoubleClickEvent() #Na dvoklik da se otvori aktivirani plugin 
        self.main_layout.addWidget(self.text_widget)
    
    def create_video_slot(self):
        self.video_widget = VideoWidget(self.video_path)
        self.video_widget.show()
        self.main_layout.addWidget(self.video_widget)

    def create_audio_slot(self):
        self.audio_widget = AudioWidget(self.audio_path)
        self.audio_widget.show()
        self.main_layout.addWidget(self.audio_widget)

    def open_plugin_tab(self):
        print("Prelazak na rad sa zeljenim elementom")

    def create_image_slot(self):
        
        self.image_label = QLabel()
        #self.image_label.setBackgroundRole(QPalette.Base)
        self.image = QImage("C:/Users/User/Desktop/MRS projekat/mrs-2022-rukovalac-dokumentima/resources/icons/image_icon.png")
        #self.image.scaled(100, 100)

        self.image_label.setPixmap(QPixmap.fromImage(self.image).scaled(300, 200))

        #self.image_label.mouseDoubleClickEvent()

        self.main_layout.addWidget(self.image_label)  #Test
    
    def delete_slot(self):
        print("pozvna delete slot metoda")
        widget = self.main_layout.removeItem() #.findChild(QtWidgets.QTextEdit)
        self.main_layout.removeWidget(widget)
        print(widget)

    def populate_tool_bar(self):
        self.tool_bar.addAction(self.toolbar_actions["text"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.toolbar_actions["image"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.toolbar_actions["delete"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.toolbar_actions["video"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.toolbar_actions["audio"])

    
    def bind_actions(self):
        self.toolbar_actions["text"].triggered.connect(self.create_text_slot)
        self.toolbar_actions["image"].triggered.connect(self.create_image_slot)
        self.toolbar_actions["delete"].triggered.connect(self.delete_slot)
        self.toolbar_actions["video"].triggered.connect(self.create_video_slot)
        self.toolbar_actions["audio"].triggered.connect(self.create_audio_slot)


    # def show_text(self, text):
    #     self.main_text.setText(text)