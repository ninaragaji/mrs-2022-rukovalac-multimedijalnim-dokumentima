from PySide2 import QtWidgets
from PySide2.QtCore import QUrl, Qt
from PySide2.QtMultimedia import QMediaPlayer, QMediaContent
from PySide2.QtMultimediaWidgets import QVideoWidget
from PySide2.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QSlider


class AudioWidget(QWidget):

    def __init__(self,audio_path, parent=None):
        super().__init__(parent)
        # Create the video widget and media player
        self.audio_widget = QVideoWidget(self)
        self.audio_widget.setFixedHeight(0)
        self.audio_widget.setFixedWidth(0)
        self.player = QMediaPlayer(self)
        self.player.setVideoOutput(self.audio_widget)
        self.player.setMedia(QMediaContent(QUrl.fromLocalFile(audio_path)))

        play_button = QPushButton('Play')
        play_button.clicked.connect(self.play)
        pause_button = QPushButton("Pause")
        pause_button.clicked.connect(self.pause)
        stop_button = QPushButton("Stop")
        stop_button.clicked.connect(self.stop)

        # Set up the layout
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0,20,0,0)
        layout.addWidget(play_button)
        layout.addWidget(pause_button)
        layout.addWidget(stop_button)
        volumeControl = QHBoxLayout()
        layout.addLayout(volumeControl)

        btnVolumeUp = QPushButton('+')
        btnVolumeUp.clicked.connect(self.volumeUp)
        btnVolumeDown = QPushButton('-')
        btnVolumeDown.clicked.connect(self.volumeDown)
        btnVolumeMute = QPushButton('Mute')
        btnVolumeMute.clicked.connect(self.volumeMute)
        volumeControl.addWidget(btnVolumeUp)
        volumeControl.addWidget(btnVolumeMute)
        volumeControl.addWidget(btnVolumeDown)
        layout.addWidget(self.audio_widget)

    def volumeUp(self):
        currentVolume = self.player.volume()
        self.player.setVolume(currentVolume + 15)

    def volumeDown(self):
        currentVolume = self.player.volume()
        self.player.setVolume(currentVolume - 15)

    def volumeMute(self):
        self.player.setMuted(not self.player.isMuted())

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()

    def stop(self):
        self.player.stop()