import os, json
from tkinter import W
from PySide2 import QtWidgets, QtGui, QtCore
from ui.manual_window import ManualWindow

from ui.structure_dock import StructureDock
from ui.workspace import WorkspaceWidget
from .plugin_manager import PluginManager
from datetime import datetime
import os

from .popup_window import PopupWindow


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, config, parent=None):
        super().__init__(parent)
        self.config = config

        self.setWindowTitle(self.config["title"])
        self.setWindowIcon(QtGui.QIcon(self.config["icon"]))
        self.resize(self.config["window_size"]["width"], self.config["window_size"]["height"])

        # registar plugin-ova
        self.plugin_registry = None

        #meni
        self.menu_bar = QtWidgets.QMenuBar(self)

        #view
        # self.view_menu = QtWidgets.QMenu("View")

        #toolbar
        self.tool_bar = QtWidgets.QToolBar("Toolbar",self)

        #statusbar
        self.status_bar = QtWidgets.QStatusBar(self)
        self.status_bar.setStyleSheet("border: 1px solid black; background-color: #D6D7D6")

            #statusbar date and time
        self.status_bar_label_time = QtWidgets.QLabel("Datum i vreme:( " + str(datetime.today().strftime('%d/%m/%Y %H:%M:%S') + " )    /  "))
        self.status_bar_label_time.setStyleSheet("border: 0")
        self.status_bar.addPermanentWidget(self.status_bar_label_time)

            #statusbar user
        self.loggedPerson = self.check_logged_user()
        self.status_bar_user = QtWidgets.QLabel('Korisnik < ' + self.loggedPerson + ' >    /  ')
        self.status_bar_user.setStyleSheet("border :0")
        self.status_bar.addPermanentWidget(self.status_bar_user)

            #statusbar action
        self.status_bar_action = QtWidgets.QLabel('Akcija < Naziv komandne akcije >    /  ')
        self.status_bar_action.setStyleSheet("border :0")
        self.status_bar.addPermanentWidget(self.status_bar_action)

            #statusbar status
        self.status_bar_status = QtWidgets.QLabel('Status < Ready >')
        self.status_bar_status.setStyleSheet("border :0")
        self.status_bar.addPermanentWidget(self.status_bar_status)

        #centralwidget
        self.central_widget = QtWidgets.QTabWidget(self)
        self.workspace = WorkspaceWidget(self)

        self.structure_dock = StructureDock("Workspace manager", self)
    

        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/Quit.png"), "&Quit", self),
            "plugin_manager": QtWidgets.QAction(QtGui.QIcon("resources/icons/document.png"), "&Plugin Manager", self),
            "new": QtWidgets.QAction(QtGui.QIcon("resources/icons/newFile.png"), "&New file", self),
            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/savefile.jpg"), "&Save", self),
            "open": QtWidgets.QAction(QtGui.QIcon("resources/icons/openFile.png"), "&Open", self),
            "undo": QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"),"&Undo", self),
            "redo": QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"),"&Redo", self),
            "manual": QtWidgets.QAction("&Manual", self),
            "about": QtWidgets.QAction("&About", self),
            "reports": QtWidgets.QAction("&Reports", self)
            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }

        self._bind_actions()

        # self._populate_menu_bar()

        # self.setMenuBar(self.menu_bar)
        # self.addToolBar(self.tool_bar)
        # self.setStatusBar(self.status_bar)
        #self.setCentralWidget(self.central_widget)


        self.populate_main_window()

        #Video plugin
        self.video_extensions = ['.avi','.mov','.mp4','.mkv','.wmv','.flv','.m4v','.webm']

        #Audio extensions
        self.audio_extensions = ['.mp3','.aac','.flac','.alac','.wav','.aiff','.dsd']

    def populate_main_window(self):
        self._populate_menu_bar()
        self._populate_tool_bar()
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock)

        self._populate_text_widget()

     
        self.central_widget.setTabsClosable(True)
        self.central_widget.tabCloseRequested.connect(self.delete_tab)
        self.setCentralWidget(self.central_widget)
        self.central_widget.addTab(self.workspace, "Welcome")

        # self.central_widget.setTabsClosable(True)
        # self.central_widget.tabCloseRequested.connect(self.delete_tab)
    

    def _populate_text_widget(self):
        """
            Populisati prilikom ucitavanja konteksta, kreirati sve tabove koji su bili otvoreni
            sa widgetima. Podesiti modele za svaki ucitani widget.

        """
        text_editor_wgt = QtWidgets.QTextEdit(self)
        text_editor_wgt.setText("Ovo je text editor!!!")
        self.setCentralWidget(text_editor_wgt)


    def read_file(self, index):
        path = self.structure_dock.model.filePath(index)
        filename, file_extension = os.path.splitext(path)
        video_plugin = self.plugin_registry.get_plugin_by_name('Video editor plugin-1.0.0')
        audio_plugin = self.plugin_registry.get_plugin_by_name('Audio editor plugin-1.0.0')
        if (file_extension in [s for s in self.video_extensions]):
            if(video_plugin.activated):
                #TODO: Otvoriti video
                new_workspace = WorkspaceWidget(self.central_widget, path)
                self.central_widget.addTab(new_workspace, "Page" + "/" + path.split("/")[-1])
            else:
                self.popup_window = PopupWindow('Upozorenje', 'Video plugin nije aktiviran!')
        elif(file_extension in [s for s in self.audio_extensions]):
            if(audio_plugin.activated):
                new_workspace = WorkspaceWidget(self.central_widget, None,path)
                self.central_widget.addTab(new_workspace, "Page" + "/" + path.split("/")[-1])
            else:
                self.popup_window = PopupWindow('Upozorenje', 'Audio plugin nije aktiviran!')
        else:
            with open(path) as f:
                text = f.read()
                new_workspace = WorkspaceWidget(self.central_widget)
                self.central_widget.addTab(new_workspace, "Page" + "/" + path.split("/")[-1])
                new_workspace.show_text(text)
    

    def save_file(self):
        name = QtWidgets.QFileDialog.getSaveFileName(self, "Save")[0]
        print(name)
        file = open(name + ".txt", 'w')
        text = self.workspace.main_text.toPlainText()
        print(text)
        file.write(text)
        file.close()


    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File", self.menu_bar)
        edit_menu = QtWidgets.QMenu("&Edit", self.menu_bar)
        view_menu = QtWidgets.QMenu("&View", self.menu_bar)
        plugins_menu = QtWidgets.QMenu("&Plugins", self.menu_bar)
        help_menu = QtWidgets.QMenu("&Help", self.menu_bar)

        file_menu.addAction(self.actions_dict["open"])
        file_menu.addAction(self.actions_dict["new"])
        file_menu.addAction(self.actions_dict["save"])
        file_menu.addAction(self.actions_dict["quit"])

        edit_menu.addAction(self.actions_dict["undo"])
        edit_menu.addAction(self.actions_dict["redo"])

        toggle_structure_dock = self.structure_dock.toggleViewAction()
        view_menu.addAction(toggle_structure_dock)
        
        plugins_menu.addAction(self.actions_dict["plugin_manager"])

        help_menu.addAction(self.actions_dict["manual"])

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(edit_menu)
        self.menu_bar.addMenu(view_menu)
        self.menu_bar.addMenu(plugins_menu)
        self.menu_bar.addMenu(help_menu)
    

    def _populate_tool_bar(self):
        self.tool_bar.addAction(self.actions_dict["new"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.actions_dict["save"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.actions_dict["undo"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.actions_dict["redo"])
        self.tool_bar.addSeparator()



    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)
        self.actions_dict["plugin_manager"].triggered.connect(self.open_plugin_manager)
        
        self.actions_dict["save"].triggered.connect(self.save_file)
        self.actions_dict["manual"].triggered.connect(self.open_manual)


        self.structure_dock.tree.clicked.connect(self.read_file)

    def open_manual(self):
        manual = ManualWindow()
        manual.exec_()


    def add_plugin_registry(self, registry):
        self.plugin_registry = registry


    def open_plugin_manager(self):
        manager = PluginManager(self, self.plugin_registry)
        manager.show()


    def set_central_widget(self, name: str):
        """
        Podesava centralni widget glavnog prozora, na osnovu simboličkog imena se dobija plugin
        koji će se smestiti u centralni deo glavnog prozora.

        :param symbolic_name: Simbolicko ime plugina koji želimo da instanciramo.
        """
        plugin = self.plugin_registry.get_by_name(name)
        widgets = plugin.get_widget()
      
        self.central_widget.addTab(widgets,  "Page" + "/" + name.split("/")[-1])
        # if widgets[1] is not None:
            
        #     self.tool_bar.addSeparator()
        #     self.tool_bar.addActions(widgets[1].actions())
        #self.menubar.addMenu(widgets[2]) if widgets[2] is not None else None
        


    def delete_tab(self, index):
        self.central_widget.removeTab(index)
        print("Obrisan tab")


    
    # *************************************** #
    # Metode koje ce koristiti drugi widget-i #
    # *************************************** #
    # TODO: proveriti koje metode bi jos bile od znacaja
    # def set_status_message(self, message=""):
    #     self.status_bar.clearMessage()
    #     self.status_bar.showMessage(message)

    def add_menu_action(self, menu_name, action):
        menues = self.menu_bar.findChildren(QtWidgets.QMenu)
        for menu in menues:
            if menu.title() == menu_name:
                menu.addAction(action)
                break
    
    def remove_menu_action(self, menu_name, action):
        menues = self.menu_bar.findChildren(QtWidgets.QMenu)
        for menu in menues:
            if menu.title() == menu_name:
                menu.removeAction(action)
                break

    def add_menu(self, menu):
        self.menu_bar.addMenu(menu)

    def delete_tab(self, index):
        self.central_widget.removeTab(index)
        #print("Obrisan tab")

    # Vracanje korisnika koji je ulogovan
    def check_logged_user(self):
        return self.check_configuration_file_for_logged_person()

    # Provera da li je autorizacija omogucena. Ako jeste provera ko je prijavljen
    def check_configuration_file_for_logged_person(self, path="configuration.json"):
        with open(path, "r", encoding="utf-8") as fp:
            data = json.load(fp)
        if data["authorization"] == True:
            return data["loggedPerson"]
        else:
            return ""
