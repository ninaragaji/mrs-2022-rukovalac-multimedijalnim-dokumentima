from PySide2 import QtWidgets
from PySide2.QtCore import QUrl
from PySide2.QtMultimedia import QMediaPlayer, QMediaContent
from PySide2.QtMultimediaWidgets import QVideoWidget
from PySide2.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout


class VideoWidget(QWidget):

    def __init__(self,video_path, parent=None):
        super().__init__(parent)
        # Create the video widget and media player
        self.video_widget = QVideoWidget(self)
        self.media_player = QMediaPlayer(self)
        self.media_player.setVideoOutput(self.video_widget)

        self.media_player.setMedia(QMediaContent(QUrl.fromLocalFile(video_path)))

        # Create the control buttons
        self.play_button = QPushButton("Play")
        self.play_button.clicked.connect(self.play)
        self.pause_button = QPushButton("Pause")
        self.pause_button.clicked.connect(self.pause)
        self.stop_button = QPushButton("Stop")
        self.stop_button.clicked.connect(self.stop)


        # Set up the layout
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0,20,0,0)
        layout.addWidget(self.video_widget)
        layout.addWidget(self.play_button)
        layout.addWidget(self.pause_button)
        layout.addWidget(self.stop_button)

    def play(self):
        self.media_player.play()

    def pause(self):
        self.media_player.pause()

    def stop(self):
        self.media_player.stop()