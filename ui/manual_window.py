from PySide2.QtWidgets import QLabel
from PySide2 import QtWidgets
class ManualWindow(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Manual")
        self.resize(500,500)
        manual_text = "\t\t    Upustvo za rad sa aplikacijom 'Rukovalac dokumentima'\n\n\n"
        manual_text += "'Rukovalac dokumentima' je aplikacija koja omogućava upravljanje radnim prostorima sa dodatnim funkcionalnostima koje "
        manual_text += "omogućavaju različiti plugini."
        manual_text += "Korisnik može da upravlja različitim vrstama fajlova, kao i da aktivira i deaktivira različite plugine "
        manual_text += "koji će mu omogućiti da proširi funkcionalnost aplikacije prema njegovim potrebama. "
        manual_text += "Korišćenje aplikacije je jednostavno i intuitivno, a korisnik će brzo savladati sve funkcije.\n\n"
        manual_text += "Za početak rada sa aplikacijom, korisnik mora da bude prijavljen na sistem. Korisnik se prijavljuje na sistem "
        manual_text += "upisivanjem korisničkog imena i šifre u prvom prozoru koji mu se otvara pri pokretanju aplikacije i klikom na dugme 'Submit'.\n\n"
        manual_text += "Ulogovani korisnik može klikom na 'Plugins' i zatim na 'Plugin manager' otvoriti prozor sa svim dostupnim pluginovima."
        manual_text += "Klikom na plugin koji mu je od interesa, može isti da aktivira klikom na dugme 'Activate', takođe već aktiviran plugin "
        manual_text += "korisnik može deaktivirati klikom na dugme 'Deactivate'. \n\n"
        manual_text += "Korisnik može otvoriti stranicu, navođenjem do lokacije fajla putem 'Workspace managera' koji je sa leve strane glavnog porozora "
        manual_text += "a nakon toga klkom na isti. Ovom akcijom otvara se nova stranica.\n\n"
        manual_text += "Na otvorenoj stranici korisnik može manipulisati željenim fajlom otvaranjem 'slota'. Korisnik otvara novi 'slot' na stranici "
        manual_text += "klikom na željenu ikonicu u gornjem levom uglu otvorene stranice.\n\n"
        manual_text += "Korišćenje svakog plugina će varirati, ali su svi jednostavni za korišćenje. "
        manual_text += "Ukoliko ima bilo kakvih problema ili pitanja, korisnik može da se obrati podršci aplikacije. Da bi to uradio, korisnik mora "
        manual_text += "prvo aktivirati 'Reports' plugin, a zatim klikom na glavnom ekranu na 'Help' pa na 'Reports' može proslediti svoju poruku.\n\n\n"
        manual_text += "Zahvaljujemo se što koristite aplikaciju 'Rukovalac dokumentima' i nadamo se da će Vam pomoći da efikasnije obavljate svoje zadatke."

        label = QLabel(manual_text)
        label.setWordWrap(True)
        layout = QtWidgets.QGridLayout()
        layout.addWidget(label)
        self.setLayout(layout)