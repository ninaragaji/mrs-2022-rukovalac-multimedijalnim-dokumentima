import sys, json
from PySide2 import QtWidgets, QtGui
from plugin_framework.plugin_registry import PluginRegistry
from ui.main_window import MainWindow
from plugins.login_plugin.widgets.login_widget import LoginWidget

def _load_configuration(path="configuration.json"):
    with open(path, "r", encoding="utf-8") as fp:
        config = json.load(fp)
        return config

def _show_main_window():
    main_window = MainWindow(config)
    plugin_registry = PluginRegistry("plugins", main_window)
    main_window.add_plugin_registry(plugin_registry)
    main_window.show()

if __name__ == "__main__":
    config = _load_configuration()
    application = QtWidgets.QApplication(sys.argv)
    if config["authorization"] == True:
        login_window = LoginWidget(config)
        login_window.show()    
    else:
        _show_main_window()

    sys.exit(application.exec_())
