from plugin_framework.extension import Extension
from plugins.audio_plugin.audio_widget import AudioEditor


class Plugin(Extension):

    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        print("Audio plugin initialized")

    def activate(self):
        self.activated = True
        print("Activated")

    def deactivate(self):
        self.activated = False

    def get_widget(self, parent=None):
        return AudioEditor(parent)