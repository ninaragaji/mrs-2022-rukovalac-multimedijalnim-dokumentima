from plugin_framework.extension import Extension
from .video_widget import VideoEditor


class Plugin(Extension):

    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        print("Text editor initialized")

    def activate(self):
        self.activated = True
        print("Activated")

    def deactivate(self):
        self.activated = False

    def get_widget(self, parent=None):
        return VideoEditor(parent)