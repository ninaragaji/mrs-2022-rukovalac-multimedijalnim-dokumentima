import qdarkstyle
from PySide2.QtCore import QFile, QTextStream, Qt
from PySide2.QtGui import QPalette, QColor
from PySide2.QtWidgets import QApplication
from plugin_framework.extension import Extension


class Plugin(Extension):

    def __init__(self, specification, iface):
        super().__init__(specification, iface)
        print("Dark mode initialized")

    def activate(self):
        self.activated = True
        app = QApplication.instance()
        if app is None:
            raise RuntimeError("No Qt Application found.")
        app.setStyleSheet(qdarkstyle.load_stylesheet_pyside2())
        print("Activated")

    def deactivate(self):
        app = QApplication.instance()
        if app is None:
            raise RuntimeError("No Qt Application found.")
        app.setStyleSheet("")
        self.activated = False