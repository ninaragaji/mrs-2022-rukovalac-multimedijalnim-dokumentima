from plugin_framework.extension import Extension
from .widgets.login_widget import LoginWidget
import json

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = LoginWidget(iface)
        print("Login plugin initialized!")

    def change_configuration_file(self, value, path="configuration.json"):
        with open(path, "r", encoding="utf-8") as fp:
            data = json.load(fp)
        with open(path, "w", encoding="utf-8") as fp:
            data["authorization"] = value
            json.dump(data, fp, indent=3)

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.activated = True
        self.change_configuration_file(True)
        print("Activated")

    def deactivate(self):
        self.activated = False
        self.change_configuration_file(False)
        print("Deactivated")