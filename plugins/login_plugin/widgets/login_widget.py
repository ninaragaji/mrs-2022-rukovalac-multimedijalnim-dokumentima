from PySide2 import QtWidgets
from ui.main_window import MainWindow
from plugin_framework.plugin_registry import PluginRegistry
import csv, json

def _show_main_window(config):
    main_window = MainWindow(config)
    plugin_registry = PluginRegistry("plugins", main_window)
    main_window.add_plugin_registry(plugin_registry)
    main_window.show()

class LoginWidget(QtWidgets.QWidget):
    config_path = "configuration.json"
    def __init__(self, config, parent=None):
        self.config = config
        super().__init__(parent)
        self.setWindowTitle('Login')
        self._populate_login_window()

    def _populate_login_window(self):
        layout = QtWidgets.QVBoxLayout()

        # labels
        self.txt_username = QtWidgets.QLineEdit()
        self.txt_password = QtWidgets.QLineEdit()

        # Sakrivanje prikaza lozinke
        self.txt_password.setEchoMode(QtWidgets.QLineEdit.Password)

        layout.addWidget(QtWidgets.QLabel('Username:'))
        layout.addWidget(self.txt_username)
        layout.addWidget(QtWidgets.QLabel('Password:'))
        layout.addWidget(self.txt_password)

        # button submit 
        submit = QtWidgets.QPushButton("Submit")
        layout.addWidget(submit)
        submit.clicked.connect(self._login)

        self.setFixedSize(300,150)
        self.setLayout(layout)

    # Provera da li korisnik sa unesenim kredencijalima postoji
    def _login(self):
        with open('data/users.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    print(self.txt_username.text())
                    print(self.txt_password.text())
                    print(row)
                    if str(self.txt_username.text()) == str(row[1]):
                        if str(self.txt_password.text()) == str(row[2]):
                            print('Login successful')
                            self._write_logged_person(str(row[3]))
                            _show_main_window(self.config)
                            self.hide()
            self.txt_username.setText('')
            self.txt_password.setText('')
            print('User not found')

    #  Upisivanje ulogovane osobe
    def _write_logged_person(self, value, path="configuration.json"):
        with open(path, "r", encoding="utf-8") as fp:
            data = json.load(fp)
        with open(path, "w", encoding="utf-8") as fp:
            data["loggedPerson"] = value
            json.dump(data, fp, indent=3)