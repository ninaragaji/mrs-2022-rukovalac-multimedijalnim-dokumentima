from PySide2 import QtWidgets, QtGui
from PySide2.QtWidgets import QFileDialog


class TextEditor(QtWidgets.QWidget):

    def __init__(self, parent):
        super().__init__(parent)

        self.text_edit_layout = QtWidgets.QVBoxLayout()
        self.text_editor = QtWidgets.QTextEdit(self)
        self.tool_bar = QtWidgets.QToolBar("Tool bar", self)


        self.tool_actions = {
            "Open": QtWidgets.QAction(QtGui.QIcon("resources/icons/newFile.png"), "&Open file"),
            "Save": QtWidgets.QAction(QtGui.QIcon("resources/icons/savefile.jpg"), "&Save"),
            "Undo": QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"), "&Undo"),
            "Redo": QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"), "&Redo"),
            "Font": QtWidgets.QAction(QtGui.QIcon("resources/icons/font.png"), "&Font"),
        }
    
        self.populate_tool_bar()
        self.bindActions()

        self.text_edit_layout.addWidget(self.tool_bar)
        self.text_edit_layout.addWidget(self.text_editor)

        self.setLayout(self.text_edit_layout)


    def populate_tool_bar(self):
        self.tool_bar.addAction(self.tool_actions["Open"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.tool_actions["Save"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.tool_actions["Undo"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.tool_actions["Redo"])
        self.tool_bar.addSeparator()
        self.tool_bar.addAction(self.tool_actions["Font"])


    def bindActions(self):
        self.tool_actions["Open"].triggered.connect(self.open_file)
        self.tool_actions["Save"].triggered.connect(self.save_file)
        self.tool_actions["Undo"].triggered.connect(self.text_editor.undo)
        self.tool_actions["Redo"].triggered.connect(self.text_editor.redo)
        self.tool_actions["Font"].triggered.connect(self.font_dialog)

 
    def open_file(self):
        file_name = QtWidgets.QFileDialog.getOpenFileName(self, "Open file")
        with open(file_name[0], "r") as fp:
            text = fp.read()
            self.text_editor.setText(text)

    def save_file(self):
        name = QtWidgets.QFileDialog.getSaveFileName(self, "Save")[0]
        print(name)
        file = open(name + ".txt", "w")
        text = self.text_editor.toPlainText()
        print(text)
        file.write(text)
        file.close()

    
    def font_dialog(self):
        (ok, font) = QtWidgets.QFontDialog.getFont()
        if ok:
            self.text_editor.setFont(font)