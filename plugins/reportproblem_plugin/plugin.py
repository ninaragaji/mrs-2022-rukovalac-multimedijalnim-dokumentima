from plugin_framework.extension import Extension
from PySide2 import QtWidgets

from .widgets.reports_widget import Reports

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = Reports(iface)
        self.open_action = QtWidgets.QAction("&Reports")
        self.open_action.triggered.connect(self.open_reports)

        print("Report problem plugin initialized!")

    def activate(self):
        self.activated = True
        self.iface.add_menu_action("&Help", self.open_action)
        print("Activated")

    def deactivate(self):
        self.activated = False
        self.iface.remove_menu_action("&Help", self.open_action)
        print("Deactivated")

    def open_reports(self):
        self.widget.show()