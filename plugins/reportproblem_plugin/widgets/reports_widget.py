from PySide2 import QtWidgets, QtCore
import os

class Reports(QtWidgets.QDialog):
    config_path = "configuration.json"
    def __init__(self, parent=None):
        super().__init__(parent)
        self._populate_layout()
        self.setLayout(self._layout)
    
        self.setWindowTitle("Reporting a problem")
        self.resize(300, 150)


    def _populate_layout(self):
        self._layout = QtWidgets.QVBoxLayout()

        self._textbox = QtWidgets.QTextEdit()

        self._label = QtWidgets.QLabel("What kind of issues are you experiencing?")
        self._label.setAlignment(QtCore.Qt.AlignCenter)

        self._submit = QtWidgets.QPushButton("Submit")
        self._submit.clicked.connect(self._save_problem)

        self._layout.addWidget(self._label)
        self._layout.addWidget(self._textbox)
        self._layout.addWidget(self._submit)

    def _save_problem(self):
        if os.path.getsize('./data/problems.txt') == 0:
            self._write_problem(str(self._textbox.toPlainText()))
        else:
            problemToWrite = "\n" + str(self._textbox.toPlainText())
            self._write_problem(problemToWrite)

    def _write_problem(self, problemToWrite):
        with open('./data/problems.txt', 'a') as txt_file_write:
            txt_file_write.write(problemToWrite)
            self._textbox.setText('')
            self.hide()
            QtWidgets.QMessageBox.information(self, 'Problem reported', 'Thank you for your help and support.\nOur developers will be notifed right away.', QtWidgets.QMessageBox.Close)
            
