from PySide2 import QtWidgets
from plugin_framework.extension import Extension
from .image_viewer_widget import ImageViewer


class Plugin(Extension):

    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)

        print("Image Viwer initialized!")


    def activate(self):
        self.activated = True
        print("Activated")
    

    def deactivate(self):
        self.activated = False
    

    def get_widget(self, parent=None):
        return ImageViewer(parent)#, None, None